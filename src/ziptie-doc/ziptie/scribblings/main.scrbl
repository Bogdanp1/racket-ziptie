;; This file is part of racket-ziptie - Racket library for unusual scenarios.
;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ziptie is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ziptie is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ziptie.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require ziptie/git/hash)


@title[#:tag "ziptie"]{Ziptie}

@author[@author+email["Maciej Barć" "xgqt@riseup.net"]]


Racket library for unusual scenarios.

Dirty tricks to get your Racket code running in unusual scenarios.

ZipTie allows you to more easily and securely bind/fuse/glue together Racket
and external build or runtime dependencies.


@(define upstream-tree
   "https://gitlab.com/xgqt/racket-ziptie/-/tree/")

Commit hash:
@(let ([git-hash (git-get-hash #:short? #t)])
   (case git-hash
     [("N/A")
      (displayln "[WARNING] Not inside a git repository!")
      git-hash]
     [else
      (link (string-append upstream-tree git-hash) git-hash)]))


@table-of-contents[]

@include-section{completion.scrbl}

@index-section[]
