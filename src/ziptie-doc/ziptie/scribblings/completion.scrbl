;; This file is part of racket-ziptie - Racket library for unusual scenarios.
;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ziptie is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ziptie is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ziptie.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket/cmdline)
          (only-in scribble/bnf nonterm))


@title[#:tag "ziptie-completion"]{Ziptie-Completion}

@exec{ziptie-make-completion} converts output of selected executable help
page into a completion plugin for a specified interactive shell.

Help output (for @Flag{h} or @DFlag{help}) of commands differ,
so the only one supported right now is the Racket-style one created by using
the @racketid[racket/cmdline] library's @racket[command-line] macro.


@section[#:tag "ziptie-completion-synopsis"]{Synopsis}

@exec{ziptie-make-completion} invocation the following form is accepted:

@exec{ziptie-make-completion [option] ... <executable>}

The completion plugin contents are printed to the standard output
(most likely the terminal window), so to put them into a file the output
has to be redirected into the specified file,
for example:

@nested[#:style 'code-inset]{@verbatim{
 ziptie-make-completion --completion-type zsh ziptie-make-completion \
     > _ziptie-make-completion
}}


@section[#:tag "ziptie-completion-flags"]{Flags}

@itemlist[
 @item{
  @Flag{t} @nonterm{type-name} or @DFlag{compeltion-type} @nonterm{type-name}
  --- pick a complewtion type to generate completions for
 }
 @item{
  @Flag{s} or @DFlag{supported}
  --- list all completion types supported by @exec{ziptie-make-completion}
 }
 ]
