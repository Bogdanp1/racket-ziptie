#lang info


(define racket-launcher-names
  '("ziptie-make-completion"))

(define racket-launcher-libraries
  '("completion/make-completion-exe.rkt"))
