;; This file is part of racket-ziptie - Racket library for unusual scenarios.
;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ziptie is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ziptie is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ziptie.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require (prefix-in port: (only-in racket/port with-output-to-string))
         (prefix-in string: (only-in racket/string string-join))
         threading
         "dependencies.rkt")

(provide make-makefile-body make-makefile-contents)


(define make-makefile-pre-body "MAKE            ?= make
RACKET          := racket
RACO            := $(RACKET) -l raco --
SCRIBBLE        := $(RACO) scribble

DO-DOCS         := --no-docs
INSTALL-FLAGS   := --auto --skip-installed $(DO-DOCS)
REMOVE-FLAGS    := --force --no-trash $(DO-DOCS)
DEPS-FLAGS      := --check-pkg-deps --unused-pkg-deps
SETUP-FLAGS     := --tidy --avoid-main $(DO-DOCS) $(DEPS-FLAGS)
TEST-FLAGS      := --heartbeat --no-run-if-absent --submodule test --table

PWD             := $(shell pwd)

all: clean compile

clean-pkg-%:
\tfind $(*) -type d -name \"compiled\" -exec rm -dr {} +
compile-pkg-%:
\t$(RACKET) -e \"(require compiler/compiler setup/getinfo) (compile-directory-zos (path->complete-path \\\"$(*)\\\") (get-info/full \\\"$(*)/info.rkt\\\") #:skip-doc-sources? #t #:verbose #f)\"
install-pkg-%:
\tcd $(*) && $(RACO) pkg install $(INSTALL-FLAGS)
setup-pkg-%:
\t$(RACO) setup $(SETUP-FLAGS) --pkgs $(*)
test-pkg-%:
\t$(RACO) test $(TEST-FLAGS) --package $(*)
remove-pkg-%:
\t$(RACO) pkg remove $(REMOVE-FLAGS) $(*)
")


(define (~target step name)
  (format "~a-pkg-~a" step name))

(define (make-makefile-body [pth (current-directory)])
  (port:with-output-to-string
   (lambda ()
     (display make-makefile-pre-body)
     (for ([step '("clean" "compile" "install" "setup" "test" "remove")])
       (define pkg-graph
         (~> pth
             get-pkgs-deps
             build-pkgs-graph
             hash->list
             (sort string<? #:key car)))
       (define targets-path
         (map car (dependency-path pkg-graph)))
       (newline)
       ;; STEP-pkg-PKG
       (for ([pkg pkg-graph])
         (let ([target (~target step (car pkg))]
               [target-dependencies
                (~>> pkg
                     cdr
                     (sort _ (lambda (A B)
                               (> (length (member A targets-path))
                                  (length (member B targets-path)))))
                     (map (lambda (str) (~target step str))))]
               [pkg-reverse-dependencies
                (reverse-dependencies pkg-graph (car pkg))])
           (cond
             [(and (equal? step "remove")
                   (not (null? pkg-reverse-dependencies)))
              (printf "~a: ~a"
                      target
                      (~>> pkg-reverse-dependencies
                           (map car)
                           (map (lambda~>> (~target step)))
                           string:string-join))
              (newline)]
             [(and (not (equal? step "remove"))
                   (not (null? target-dependencies)))
              (printf "~a: ~a"
                      target
                      (string:string-join target-dependencies))
              (newline)]
             [else
              (printf "")])))
       (~>> pkg-graph
            (map cdr)
            (filter (lambda (lst) (not (null? lst))))
            null?
            not
            (and _ (newline)))
       ;; STEP
       (define step-targets-path
         (map (lambda (str) (~target step str)) targets-path))
       (case step
         [("remove")
          (printf "~a: ~a" step
                  (string:string-join (reverse step-targets-path)))]
         [else
          (printf "~a: ~a" step (string:string-join step-targets-path))])
       (newline)))))

(define (make-makefile-contents [pth (current-directory)])
  (with-output-to-file (build-path pth "Makefile") #:exists 'replace
    (lambda ()
      (display (make-makefile-body pth)))))
