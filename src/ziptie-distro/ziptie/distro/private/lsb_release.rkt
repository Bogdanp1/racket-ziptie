;; This file is part of racket-ziptie - Racket library for unusual scenarios.
;; Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-ziptie is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-ziptie is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-ziptie.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require threading
         (prefix-in exn: racket/exn)
         (prefix-in string: (only-in racket/string string-trim))
         (prefix-in port: (only-in racket/port port->string)))

(provide (all-defined-out))


(define lsb_release-executable-path
  (make-parameter (find-executable-path "lsb_release")))


(define (lsb_release option)
  (let/cc
    return
    (unless (lsb_release-executable-path)
      (return #false))
    (with-handlers ([exn:fail?
                     (lambda (e)
                       (log-warning "lsb_release: error ~v"
                                    (exn:exn->string e))
                       (return #false))])
      (define-values (sp stdout stdin stderr)
        (subprocess #false
                    #false
                    #false
                    (lsb_release-executable-path)
                    "-s"
                    option))
      (close-output-port stdin)
      (subprocess-wait sp)
      (let ([stderr-string
             (port:port->string stderr)]
            [stdout-string
             (~> (port:port->string stdout)
                 string:string-trim
                 (string:string-trim "\""))])
        (close-input-port stdout)
        (close-input-port stderr)
        (cond
          [(not (equal? stderr-string ""))
           (log-warning "lsb_release: returned failure ~v" stderr-string)
           (return #false)]
          [(equal? stdout-string "n/a")
           (log-warning
            "lsb_release: \"lsb_release\" does not know about this property")
           (return #false)]
          [else
           (return stdout-string)])))))
