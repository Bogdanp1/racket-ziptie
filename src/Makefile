MAKE            ?= make
RACKET          := racket
RACO            := $(RACKET) -l raco --
SCRIBBLE        := $(RACO) scribble

DO-DOCS         := --no-docs
INSTALL-FLAGS   := --auto --skip-installed $(DO-DOCS)
REMOVE-FLAGS    := --force --no-trash $(DO-DOCS)
DEPS-FLAGS      := --check-pkg-deps --unused-pkg-deps
SETUP-FLAGS     := --tidy --avoid-main $(DO-DOCS) $(DEPS-FLAGS)
TEST-FLAGS      := --heartbeat --no-run-if-absent --submodule test --table

PWD             ?= $(shell pwd)

all: clean compile

clean-pkg-%:
	find $(*) -type d -name "compiled" -exec rm -dr {} +
compile-pkg-%:
	$(RACKET) -e "(require compiler/compiler setup/getinfo) (compile-directory-zos (path->complete-path \"$(*)\") (get-info/full \"$(*)/info.rkt\") #:skip-doc-sources? #t #:verbose #f)"
install-pkg-%:
	cd $(*) && $(RACO) pkg install $(INSTALL-FLAGS)
setup-pkg-%:
	$(RACO) setup $(SETUP-FLAGS) --pkgs $(*)
test-pkg-%:
	$(RACO) test $(TEST-FLAGS) --package $(*)
remove-pkg-%:
	$(RACO) pkg remove $(REMOVE-FLAGS) $(*)

clean-pkg-ziptie: clean-pkg-ziptie-git clean-pkg-ziptie-distro clean-pkg-ziptie-monorepo
clean-pkg-ziptie-doc: clean-pkg-ziptie-git

clean: clean-pkg-ziptie-git clean-pkg-ziptie-distro clean-pkg-ziptie-monorepo clean-pkg-ziptie-completion clean-pkg-ziptie-make clean-pkg-ziptie-process clean-pkg-ziptie-doc clean-pkg-ziptie

compile-pkg-ziptie: compile-pkg-ziptie-git compile-pkg-ziptie-distro compile-pkg-ziptie-monorepo
compile-pkg-ziptie-doc: compile-pkg-ziptie-git

compile: compile-pkg-ziptie-git compile-pkg-ziptie-distro compile-pkg-ziptie-monorepo compile-pkg-ziptie-completion compile-pkg-ziptie-make compile-pkg-ziptie-process compile-pkg-ziptie-doc compile-pkg-ziptie

install-pkg-ziptie: install-pkg-ziptie-git install-pkg-ziptie-distro install-pkg-ziptie-monorepo
install-pkg-ziptie-doc: install-pkg-ziptie-git

install: install-pkg-ziptie-git install-pkg-ziptie-distro install-pkg-ziptie-monorepo install-pkg-ziptie-completion install-pkg-ziptie-make install-pkg-ziptie-process install-pkg-ziptie-doc install-pkg-ziptie

setup-pkg-ziptie: setup-pkg-ziptie-git setup-pkg-ziptie-distro setup-pkg-ziptie-monorepo
setup-pkg-ziptie-doc: setup-pkg-ziptie-git

setup: setup-pkg-ziptie-git setup-pkg-ziptie-distro setup-pkg-ziptie-monorepo setup-pkg-ziptie-completion setup-pkg-ziptie-make setup-pkg-ziptie-process setup-pkg-ziptie-doc setup-pkg-ziptie

test-pkg-ziptie: test-pkg-ziptie-git test-pkg-ziptie-distro test-pkg-ziptie-monorepo
test-pkg-ziptie-doc: test-pkg-ziptie-git

test: test-pkg-ziptie-git test-pkg-ziptie-distro test-pkg-ziptie-monorepo test-pkg-ziptie-completion test-pkg-ziptie-make test-pkg-ziptie-process test-pkg-ziptie-doc test-pkg-ziptie

remove-pkg-ziptie-distro: remove-pkg-ziptie
remove-pkg-ziptie-git: remove-pkg-ziptie remove-pkg-ziptie-doc
remove-pkg-ziptie-monorepo: remove-pkg-ziptie

remove: remove-pkg-ziptie remove-pkg-ziptie-doc remove-pkg-ziptie-process remove-pkg-ziptie-make remove-pkg-ziptie-completion remove-pkg-ziptie-monorepo remove-pkg-ziptie-distro remove-pkg-ziptie-git
